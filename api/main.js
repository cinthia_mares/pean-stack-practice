// ----- Dependencies importation -------
var Project = require('./models');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

// allow cross origin
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:4200"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// body parse for post requests
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
// -----------------

var userModel = Project.models.User;

function checkUser() {
    return userModel.findOrCreate({ 
        where: {
            username: "cmares"
        }
    });
}

function createUser(userObj) {
    return userModel.findOrCreate({
        where: userObj
    });
}

function getUsers(request) {
    return userModel.findAll({});
}

function updateUser(userId, user) {
    return userModel.update(user, {
        where: { id: userId }
    });
}

function deleteUser(userId) {
    return userModel.destroy({ 
        where: {
            id: userId
        }
     });
}

Project.sequelize.sync();

app.get('/', function (req, res) {
    var userchido = "Postgres: Ahorita no joven.";
    checkUser().then((user) => {
        userchido = user;
        res.send(userchido);
    });
});

app.post('/api/users', (req, res) => {
    createUser(req.body).then(user => {
        res.send(user);
    })
    .catch(error => {
        res.send("El usuario ya existe");
    });
});

app.get('/api/users', (req, res) => {
    getUsers(req).then((users) => {
        res.send(users);
    });
});

app.get('/api/users/channel', (req, res) => {
    getUsers(req).then((users) => {
        res.send(users);
    });
});

app.put('/api/users/:id', (req, res) => {
    updateUser(req.params.id, req.body).then(user => {
        res.send(user);
    });
});

app.delete('/api/users/:id', (req, res) => {
    deleteUser(req.params.id).then(user => {
        res.send(true);
    });
});
  
app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});