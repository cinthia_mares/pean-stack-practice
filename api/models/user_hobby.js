module.exports = userHobby = (sequelize, DataTypes) => {
  const UsersHobbies = sequelize.define('users_hobbies', {
    // attributes
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true, 
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
      unique: 'unique-genre-per-post',
    },
    hobbyId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
      unique: 'unique-genre-per-post',
    }
  }, {
    // options
    timestamps: true,
    underscored: true,
    tableName: 'users_hobbies',
  });

  UsersHobbies.associate = models => {
    // Associating to User model
    UsersHobbies.belongsTo(models.User, { 
      foreignKey: 'userId',
      targetKey: 'id',
      onDelete: 'CASCADE',
      as: 'User'
    });

    // Associating to Hobby model
    UsersHobbies.belongsTo(models.Hobby, { 
      foreignKey: 'hobbyId',
      targetKey: 'id',
      onDelete: 'CASCADE',
      as: 'Hobby'
    });
  };

  return UsersHobbies;
};