module.exports = hobby = (sequelize, DataTypes) => {
  const Hobby = sequelize.define('hobby', {
    // attributes
    title: {
      type: DataTypes.STRING,
      unique: true,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
    }
  }, {
    // options
  });

  // Associating Hobby table to Users table through UsersHobbies model/table
  Hobby.associate = models => {
    Hobby.belongsToMany(models.User, { 
      through: models.UsersHobbies, 
      onDelete: 'CASCADE', 
      foreignKey: 'userId', 
      as: 'users' 
    });
  };

  return Hobby;
};