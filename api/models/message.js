module.exports = message = (sequelize, DataTypes) => {
  // table definition
  const Message = sequelize.define('message', {
    text: DataTypes.STRING,
  });

  // relationship definition
  Message.associate = models => {
    Message.belongsTo(models.User);
  };

  return Message;
};