var Sequelize = require('sequelize');

const sequelize = new Sequelize(
  'mydb',
  'postgres',
  'root',
  {
    dialect: 'postgres',
    pool: {
      maxConnections: 10,
      minConnections: 0,
      maxIdleTime:    1000
    }
  },
);

const models = {
  User: sequelize.import('./user'),
  Message: sequelize.import('./message'),
  Hobby: sequelize.import('./hobby'),
  UsersHobbies: sequelize.import('./user_hobby')
};

// iterating between models
Object.keys(models).forEach(key => {
  // associating each model
  if ('associate' in models[key]) {
    models[key].associate(models);
  }
});

var db = {};
db.models = models;
db.sequelize = sequelize;
db.Sequelize = Sequelize;
module.exports = db;
// module.exports.models;