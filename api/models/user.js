module.exports = user = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    // attributes
    username: {
      type: DataTypes.STRING,
      unique: true,
    },
  }, {
    // options
  });

  User.associate = models => {
    User.hasMany(models.Message, { onDelete: 'CASCADE' });
    User.belongsToMany(models.Hobby, { 
      onDelete: 'CASCADE', 
      as: 'hobbies', 
      through: models.UsersHobbies, 
      foreignKey: 'hobbyId' 
    });
  };

  User.findByLogin = async login => {
    let user = await User.findOne({
      where: { username: login },
    });

    if (!user) {
      user = await User.findOne({
        where: { email: login }
      });
    }
  }

  return User;
};