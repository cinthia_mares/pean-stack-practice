import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { faUserMinus, faSave } from '@fortawesome/free-solid-svg-icons';
import { UserSchema } from '../schemas/UserSchema';
// import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  // for services
  // users: any;

  // through child property
  @Input() users: Array<UserSchema>;
  @Output() deletedUser = new EventEmitter();
  @Output() savedUser = new EventEmitter();
  faUserMinus = faUserMinus;
  faSave = faSave;
  editMode: boolean = false;
  editingUser: UserSchema;

  constructor(
    // private usersService: UserService
  ) { }

  ngOnInit() {
    // -- with promise
    // this.usersService.getUsers().then(response => {
    //   this.users = response;
    // });

    // -- with observable
    // this.usersService.getUsersChannel().subscribe(users => this.users = users);
  }

  deleteUser(user) {
    this.deletedUser.emit(user);
  }

  editUser(flag: boolean, userObj) {
    this.editMode = flag;
    this.editingUser = Object.assign({}, userObj);
  }

  saveUser(user) {
    this.savedUser.emit(user);
    this.users.find(u => u.id === user.id).username = user.username;
    this.editUser(false, user);
  }
}
