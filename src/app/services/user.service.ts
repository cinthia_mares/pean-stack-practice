import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { UserSchema } from '../schemas/UserSchema';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  users: any;

  constructor(
    private httpClient: HttpClient
  ) { }

  getUsers(): Promise<UserSchema[]> {
    return this.httpClient
    .get('/api/users')
    .pipe(
      map((response: any) => <UserSchema[]>response)
    )
    .toPromise();
  }

  createUser(userObj: UserSchema): Promise<UserSchema[]> {
    return this.httpClient
    .post('/api/users', userObj)
    .pipe(
      map((response: any) => <UserSchema[]>response)
    )
    .toPromise();
  }

  getUsersChannel(): Observable<UserSchema[]> {
    return this.httpClient
    .get('/api/users/channel')
    .pipe(
      map((response: any) => <UserSchema[]>response)
    );
  }

  updateUser(user: UserSchema): Promise<any> {
    return this.httpClient
    .put(`/api/users/${user.id}`, user)
    .toPromise();
  }

  deleteUser(user: UserSchema): Promise<any> {
    return this.httpClient.delete(`/api/users/${user.id}`)
    .toPromise();
  }
}
