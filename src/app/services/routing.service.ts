import { Injectable } from '@angular/core';
import { Router, Route } from '@angular/router';
import { config } from '@fortawesome/fontawesome-svg-core';

import { IndexPageComponent } from '../index-page/index-page.component';
import { UserFormComponent } from '../user-form/user-form.component';
import { HobbiesComponent } from '../hobbies/hobbies.component';

@Injectable({
  providedIn: 'root'
})
export class RoutingService {
  private routes: any = [
    { 
      // url: /
      path: '', 
      name: 'index', 
      navText: 'Inicio',
      component: IndexPageComponent },
    { 
      // url: /users
      path: 'users', 
      name: 'user-form', 
      navText: 'Usuarios',
      component: UserFormComponent },
    { 
      // url: /hobbies
      path: 'hobbies', 
      name: 'hobbies', 
      navText: 'Pasatiempos',
      component: HobbiesComponent },
  ];

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    // console.log(this.router.config);
  }

  get navigationRoutes() {
    // var test = routerConfig.map( route => {
    //   let fixedRoute = {
    //     path: route.path
    //   }
    //   return fixedRoute;
    // });
    return this.router.config.map(route => {
      return {
        url: route["path"],
        name: route["name"],
        navText: route["navText"]
      }
    });
  }

  get appRoutes() {
    return this.routes;
  }

  // get appRoutes(): Route[] {
    // let configRoutes = this.routes.map(route => {
    //   delete route["name"];
    //   delete route["navText"];
    //   return route;
    // });
    // let configRoutes = this.routes.map(
    //   (route) => { 
    //     delete route["name"];
    //     delete route["navText"];
    //     var test: Route = route;
    //     console.log(test);
        
    //     return test;
    //   }
    // );
    // console.log(configRoutes);
    // return configRoutes;
  // }
}
