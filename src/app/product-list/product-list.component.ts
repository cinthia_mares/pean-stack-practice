import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { products } from '../products';
// import { ProductDetailsComponent } from '../product-details/product-details.component';
import { ModalConfirmComponent } from '../modal-confirm/modal-confirm.component';
import { ProductDetailsComponent } from '../product-details/product-details.component';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products = products;

  share() {
    window.alert('The product has been shared!');
  }

  onNotify() {
    window.alert('You will be notified when the product goes on sale');
  }

  show(product) {
    console.log(ProductDetailsComponent);
    
    const modalRef = this.modalService.open(ModalConfirmComponent);
    modalRef.componentInstance.modal = {
      title: product.name,
      content: `<div>Estas viendo una etiqueta div como parámetro de ésta modal.</div>`
      // content: `<app-product-details [product]="${product}" />`
    }
    // modalRef.componentInstance.confirmEmitter.subscribe(() => {
      // this.activeModal.close
    // })
    // modalRef.componentInstance.product = product;
  }

  constructor(
    private modalService: NgbModal
  ) { }

  ngOnInit() {
  }

}
