import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../interfaces/configInterface';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  constructor(
    private http: HttpClient
  ) { }

  configUrl = '../config/servicesConfig.json';
  
  getConfig() {
    return this.http.get<Config>(this.configUrl);
  }
  
}
