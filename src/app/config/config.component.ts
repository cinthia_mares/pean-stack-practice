import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../config/config.service';
import { Config } from '../interfaces/configInterface';

@Component({
  template: `<div>Config Component Template</div>`,
  selector: 'app-config'
})
export class ConfigComponent implements OnInit {

  constructor(
    private configService: ConfigService
  ) { }

  ngOnInit() {
  }

  config = {};

  showConfig() {
    this.configService.getConfig()
      .subscribe((data: Config) => this.config = { ...data });
  }

}