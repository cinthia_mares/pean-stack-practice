import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { ToastService } from '../services/toast.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css'],
})
export class UserFormComponent implements OnInit {
  userForm = new FormGroup({
    username: new FormControl(''),
  });

  // through child property
  users: any;

  constructor(
    private userService: UserService,
    private toastService: ToastService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    // get all users al beggining
    this.refreshList();

    this.userForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(3)]],
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.userForm.controls;
  }

  onSubmit() {
    this.userService.createUser(this.userForm.value)
    .then(user => {
      this.userForm.reset('');
      // though child property
      this.refreshList();
    })
    .catch(error => {
      window.alert("Error al crear usuario");
      console.log(error);
    });
  }

  onDeletedUser(user) {
    this.deleteUser(user);
  }

  onSavedUser(editedUser) {
    this.updateUser(editedUser);
  }

  refreshList() {
    this.userService.getUsers().then(users => this.users = users);
  }

  deleteUser(user) {
    this.userService.deleteUser(user).then(response => {
      if (response) {
        this.toastService.show('Usuario eliminado', {
          delay: 2000 ,
          autohide: true
        });
        this.refreshList();
      }
    });
  }

  updateUser(editedUser) {
    // console.log("about to update user : ", editedUser);
    this.userService.updateUser(editedUser);
  }

}
