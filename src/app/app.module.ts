import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router'
import { ReactiveFormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ProductListComponent } from './product-list/product-list.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { ProductAlertsComponent } from './product-alerts/product-alerts.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ModalConfirmComponent } from './modal-confirm/modal-confirm.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserListComponent } from './user-list/user-list.component';
import { ConfigComponent } from './config/config.component';
import { ToastComponent } from './toast/toast.component';
import { IndexPageComponent } from './index-page/index-page.component';
import { HobbiesComponent } from './hobbies/hobbies.component';

import { RoutingService } from './services/routing.service';

var routingService = new RoutingService();
const ROUTES = routingService.appRoutes;

@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    TopBarComponent,
    ProductAlertsComponent,
    ProductDetailsComponent,
    ModalConfirmComponent,
    UserFormComponent,
    UserListComponent,
    ConfigComponent,
    ToastComponent,
    IndexPageComponent,
    HobbiesComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      ROUTES
      // routingService.buildRoutes([
      //   { path: '', component: IndexPageComponent },
      //   { path: 'users', component: UserFormComponent },
      //   { path: 'hobbies', component: HobbiesComponent },
      //   ]
      // )
      // [
      //   { path: '', component: IndexPageComponent },
      //   { path: 'users', component: UserFormComponent },
      //   { path: 'hobbies', component: HobbiesComponent },
      // ]

      // { path: '', component: ProductListComponent },
      // { path: 'products/:productId', component: ProductDetailsComponent },
    ),
    NgbModule,
    HttpClientModule,
    FontAwesomeModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent, ToastComponent],
  entryComponents: [ModalConfirmComponent]
})
export class AppModule { }
