import { Component, OnInit, Input, ComponentFactoryResolver, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-confirm',
  templateUrl: './modal-confirm.component.html',
  styleUrls: ['./modal-confirm.component.css']
})
export class ModalConfirmComponent implements OnInit {
  vm: this;
  @Output() confirmEmitter: EventEmitter<any> = new EventEmitter();
  //@ViewChild(this.vm, {static: true}) containerHost: this;
  @Input() modal;
  //interval: any;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    public activeModal: NgbActiveModal
  ) {
    console.log(this.componentFactoryResolver);
  }

  ngOnInit() {
    this.loadComponent();
    //this.interval = setInterval(() => {
      //this.loadComponent();
    //}, 3000);
  }

  ngOnDestroy() {
    //clearInterval(this.interval);
  }

  loadComponent() {
    // this.currentAdIndex = (this.currentAdIndex + 1) % this.ads.length;
    // const adItem = this.ads[this.currentAdIndex];

    // const componentFactory = this.componentFactoryResolver.resolveComponentFactory(adItem.component);
    //const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.modal.content);

    // const viewContainerRef = this.adHost.viewContainerRef;
    // viewContainerRef.clear();

    // const componentRef = viewContainerRef.createComponent(componentFactory);
    // (<AdComponent>componentRef.instance).data = adItem.data;
  }
  
  confirm() {
    // this.confirmEmitter.emit();
    this.activeModal.close();
  }
}
