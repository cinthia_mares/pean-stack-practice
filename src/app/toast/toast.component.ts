import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastService } from '../services/toast.service';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  // template: `
  //   <ngb-toast
  //     *ngFor="let toast of toastService.toasts"
  //     [header]="toast.headertext"
  //     [class]="toast.classname"
  //     [autohide]="toast.autohide"
  //     [delay]="toast.delay || 5000"
  //     (hide)="toastService.remove(toast)"
  //   >
  //     <ng-template [ngIf]="isTemplate(toast)" [ngIfElse]="text">
  //       <ng-template [ngTemplateOutlet]="toast.textOrTpl"></ng-template>
  //     </ng-template>
    
  //     <ng-template #text>{{ toast.textOrTpl }}</ng-template>
  //   </ngb-toast>
  // `,
  styleUrls: ['./toast.component.css'],
  host: {'[class.ngb-toasts]': 'true'}
})
export class ToastComponent implements OnInit {

  constructor(
    public toastService: ToastService
  ) { }

  ngOnInit() {
  }

  isTemplate(toast) { return toast.textOrTpl instanceof TemplateRef; }

}
