import { Component } from '@angular/core';
import { RoutingService } from '../app/services/routing.service';
// import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Stackblitz Tutorial';
  navUrls = [];

  constructor (
    private routingService: RoutingService,
    // private router: Router
  ) { }
  
  ngOnInit() {
    // console.log(this.router.config);
    this.navUrls = this.routingService.navigationRoutes;
  }

}
